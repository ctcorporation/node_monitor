﻿using System.IO;

namespace Node_Patrol.Classes
{
    public class NodeMonitor
    {
        #region Members        
        private string _nodePath;
        private string _connString;
        private string _errorMessage;
        #endregion

        #region Properties
        public string ConnString
        {
            get { return _connString; }
            set { _connString = value; }
        }
        public string ResponseMessage
        {
            get;
            private set;
        }
        // Maximum Number of Files allowed in the Processing Folder
        public int HighTide
        { get; set; }

        // This interval is used to calculate the Oldest possible file. 
        public double ProcessInterval
        { get; set; }
        #endregion

        #region Constructors
        public NodeMonitor(string connString)
        {
            ConnString = connString;
        }
        #endregion

        #region Methods
        public bool IsHighTide()
        {
            var path = Path.Combine(_nodePath, "Processing");
            var dirInfo = Directory.GetFiles(path);
            if (dirInfo.Length < HighTide)
            {
                return false;
            }
            foreach (var file in dirInfo)
            {

            }

        }
        #endregion 

        #region Helper

        #endregion

    }
}