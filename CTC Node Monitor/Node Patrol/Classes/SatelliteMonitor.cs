﻿using NodeData;
using NodeData.Models;
using System;
using System.IO;

namespace Node_Patrol.Classes
{
    public class SatelliteMonitor
    {
        #region Members
        private Profile _profile;
        private Customer _customer;
        private string _connString;
        private string _errorMessage;
        #endregion

        #region Properties
        public string ConnString
        {
            get { return _connString; }
            set { _connString = value; }
        }
        public Profile Profile
        {
            get { return _profile; }
            set { _profile = value; }
        }
        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }

        public string ResponseMessage
        {
            get;
            private set;
        }
        // Maximum Number of Files allowed in the Processing Folder
        public int HighTide
        { get; set; }

        // This interval is used to calculate the Oldest possible file. 
        public double ProcessInterval
        { get; set; }
        #endregion

        #region Constructors
        public SatelliteMonitor()
        {

        }

        public SatelliteMonitor(Profile profile)
        {
            Profile = profile;
        }

        public SatelliteMonitor(Customer customer)
        {
            Customer = customer;
            if (IsHighTide())
            {

            }
        }
        #endregion

        #region Methods
        public bool IsHighTide()
        {
            var path = Path.Combine(_customer.C_PATH, "Processing");
            var dirInfo = Directory.GetFiles(path);
            if (dirInfo.Length < HighTide)
            {
                return false;
            }
            foreach (var file in dirInfo)
            {
                _errorMessage = "Satellite Processor : " + _customer.C_SATELLITE + " processing folder Tide Level is at " + dirInfo.Length + ".";

                FileInfo f = new FileInfo(file);
                if (f.CreationTime > DateTime.Now.AddMinutes(ProcessInterval))
                {
                    _errorMessage += "and files are older than the Cycle time.";
                    return true;
                }
                if (!string.IsNullOrEmpty(_connString))
                {
                    using (IHeartBeatManager hbm = new HeartBeatManager(_connString))
                    {
                        var hb = hbm.GetHeartBeat(_customer.C_ID);
                        if (hb != null)
                        {
                            if (hb.HB_OPENED != null)
                            {
                                _errorMessage += "Last Checkin: " + hb.HB_LASTCHECKIN + ". Last Operation:" + hb.HB_LASTOPERATION;
                            }
                        }
                    }
                    Guid result = Guid.Empty;
                }
                _errorMessage += Environment.NewLine + "Recommend Stop and restart process. ";

            }
            return false;
        }


        #endregion

        #region Helpers

        #endregion
    }
}
