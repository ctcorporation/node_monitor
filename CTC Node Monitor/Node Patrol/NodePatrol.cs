﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Node_Patrol
{
    public partial class NodePatrol : ServiceBase
    {
        // Timer to Check on all the Satellite are running. 
        Timer tmrSatellites = new Timer { Interval = (int)TimeSpan.FromMinutes(10).TotalMilliseconds };
        // Timer to Check on CTC Node, Inbound Processor, Outbound Processor
        Timer tmrProcessing = new Timer { Interval = (int)TimeSpan.FromMinutes(5).TotalMilliseconds };
        // Timer to run hourly reports on Processing Summary. 
        Timer tmrReporting = new Timer { Interval = (int)TimeSpan.FromHours(1).TotalMilliseconds };
        Stopwatch eventStopWatch;



        public NodePatrol()
        {
            InitializeComponent();

        }

        protected override void OnStart(string[] args)
        {
        }

        protected override void OnStop()
        {
        }
    }
}
