﻿namespace Node_Patrol
{
    partial class NodePatrolInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NodePatrolServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.NodePatrolServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // NodePatrolServiceProcessInstaller
            // 
            this.NodePatrolServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.User;
            this.NodePatrolServiceProcessInstaller.Password = null;
            this.NodePatrolServiceProcessInstaller.Username = null;
            // 
            // NodePatrolServiceInstaller
            // 
            this.NodePatrolServiceInstaller.ServiceName = "CTC Node Patrol";
            this.NodePatrolServiceInstaller.Description = "This service ensures CTC Node and all Satellite applications are running by checking the number of incoming files in each of the processing folders.";
            this.NodePatrolServiceInstaller.DisplayName = "CTC Node Patrol";
            // 
            // NodePatrolInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.NodePatrolServiceProcessInstaller,
            this.NodePatrolServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller NodePatrolServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller NodePatrolServiceInstaller;
    }
}