﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace Node_Patrol
{
    [RunInstaller(true)]
    public partial class NodePatrolInstaller : System.Configuration.Install.Installer
    {
        public NodePatrolInstaller()
        {
            InitializeComponent();
        }
    }
}
